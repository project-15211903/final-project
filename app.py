from flask import Flask, request, render_template, redirect, url_for, session, flash, jsonify
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from food_recommendation_model import calculate_calories, generate_weekly_recommendations, nutrition_data
import os
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app, resources={r"/submit": {"origins": "http://127.0.0.1:5000"}})

app.static_folder = 'static'

# Configure the database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = os.urandom(24)
db = SQLAlchemy(app)

# Define User model
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    email = db.Column(db.String(120), unique=True)
    password_hash = db.Column(db.String(128))

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

class Activity(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sex = db.Column(db.String(10))
    age = db.Column(db.Integer)
    nationality = db.Column(db.String(100))
    weight = db.Column(db.Float)
    height = db.Column(db.Float)
    activity_level = db.Column(db.String(30))

# Route for the home page
@app.route("/")
def home():
    return render_template("index.html")

# Route for user registration
@app.route("/register", methods=["GET", "POST"])
def register():
    if request.method == "POST":
        first_name = request.form.get("first_name")
        last_name = request.form.get("last_name")
        email = request.form.get("email")
        password = request.form.get("password")

        if not first_name or not last_name or not email or not password:
            flash("All fields are required.")
            return render_template("register.html")

        existing_user = User.query.filter_by(email=email).first()
        if existing_user:
            flash("Email already registered")
            return render_template("register.html")
        
        new_user = User(first_name=first_name, last_name=last_name, email=email)
        new_user.set_password(password)

        try:
            db.session.add(new_user)
            db.session.commit()
            flash("Registration Successful")
            return redirect(url_for('login'))
        except Exception as e:
            db.session.rollback()
            flash(f"An error occurred: {str(e)}")
            return render_template("register.html")

    return render_template("register.html")

# Route for user login
@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        email = request.form.get("email")
        password = request.form.get("password")

        if not email or not password:
            flash("Email and password are required.")
            return render_template("login.html")

        user = User.query.filter_by(email=email).first()
        if user and user.check_password(password):
            session['user_id'] = user.id
            flash("Login Successful")
            return redirect(url_for('home'))
        else:
            flash("Invalid email or password")
            return render_template("login.html")

    return render_template("login.html")

# Route for user logout
@app.route("/logout")
def logout():
    session.pop('user_id', None)
    flash("You have been logged out.")
    return redirect(url_for('home'))

# Route for test data submission
@app.route("/index", methods=["POST"])
def index():
    sex = request.form.get("sex")
    age = request.form.get("age")
    nationality = request.form.get("nationality")
    weight = request.form.get("weight")
    height = request.form.get("height")
    activity_level = request.form.get("activity")

    if not sex or not age or not nationality or not weight or not height or not activity_level:
        return "All fields are required"

    new_activity = Activity(sex=sex, age=int(age), nationality=nationality, weight=float(weight), height=float(height), activity_level=activity_level)
    
    try:
        db.session.add(new_activity)
        db.session.commit()
        return "Data Uploaded Successfully"
    except Exception as e:
        db.session.rollback()
        return f"An error occurred: {str(e)}"
@app.after_request
def add_cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = 'http://127.0.0.1:5000'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
    response.headers['Access-Control-Allow-Methods'] = 'POST'
    return response

# Route to handle form submission and render weekly recommendation
@app.route("/submit", methods=["POST"])
@cross_origin()
def submit():
    age = request.form.get("age")
    sex = request.form.get("sex")
    weight = request.form.get("weight")
    height = request.form.get("height")
    activity_level = request.form.get("activity")

    if not age or not sex or not weight or not height or not activity_level:
        flash('All fields are required.', 'error')
        return redirect(url_for('home'))

    daily_calories = calculate_calories(int(age), sex, float(weight), float(height), activity_level)
    weekly_food = generate_weekly_recommendations(int(age), sex, float(weight), float(height), activity_level)

    return render_template('weekly_recommendation.html', daily_calories=daily_calories, weekly_food=weekly_food, nutrition_data=nutrition_data)

if __name__ == "__main__":
    with app.app_context():
        db.create_all()
    app.run(debug=True)
