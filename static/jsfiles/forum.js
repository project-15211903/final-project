document.addEventListener("DOMContentLoaded", function() {
    const form = document.getElementById("multi-step-form");
    const prevButton = document.getElementById("Prev");
    const nextButton = document.getElementById("Next");
    const submitButton = document.getElementById("Submit");
    const progressbar = document.querySelector(".indicator");
    const errorMessage = document.getElementById('error-message');
    let currentStep = 0;

    function showStep(stepIndex) {
        const sections = form.querySelectorAll("section");
        sections.forEach((section, index) => {
            section.style.display = index === stepIndex ? "block" : "none";
        });
    }

    function validateStep(stepIndex) {
        const inputs = form.querySelectorAll("section")[stepIndex].querySelectorAll("input, select");
        return Array.from(inputs).every(input => input.checkValidity());
    }

    function goToNextStep() {
        if (validateStep(currentStep)) {
            currentStep++;
            showStep(currentStep);
            updateSteps();
        } else {
            alert("Please fill out all required fields correctly.");
        }
    }

    function goToPrevStep() {
        if (currentStep > 0) {
            currentStep--;
            showStep(currentStep);
            updateSteps();
        }
    }

    function submitForm(event) {
        event.preventDefault(); // Prevent the default form submission
    
        if (validateStep(currentStep)) {
            const formData = new FormData(form);
    
            fetch('http://localhost:5000/submit', { // Adjust URL as needed
                method: 'POST',
                body: formData
            })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                if (data.success) {
                    console.log("Form submitted successfully!");
                    // Redirect to weekly_recommendation.html
                    window.location.href = "weekly_recommendation.html";
                } else {
                    console.error("Form submission error:", data.error);
                    errorMessage.textContent = data.message;
                }
            })
            .catch(error => {
                console.error("Form submission error:", error);
                errorMessage.textContent = "An unexpected error occurred.";
            });
        } else {
            alert("Please fill out all required fields correctly.");
        }
    }

    function updateSteps() {
        const circles = document.querySelectorAll(".circle");
        circles.forEach((circle, index) => {
            circle.classList.toggle("active", index <= currentStep);
        });
        progressbar.style.width = `${(currentStep / (circles.length - 1)) * 100}%`;
        prevButton.disabled = currentStep === 0;
        nextButton.style.display = currentStep === circles.length - 1 ? "none" : "block";
        submitButton.style.display = currentStep === circles.length - 1 ? "block" : "none";
    }

    // Initial setup
    showStep(currentStep);
    updateSteps();

    // Event listeners
    nextButton.addEventListener("click", goToNextStep);
    prevButton.addEventListener("click", goToPrevStep);
    form.addEventListener('submit', submitForm);
});
