import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler
from sklearn.impute import SimpleImputer
from sklearn.model_selection import KFold, cross_val_score, train_test_split
from sklearn.metrics import r2_score, mean_absolute_error
import matplotlib.pyplot as plt
import seaborn as sns

# Load and preprocess the nutrition data
nutrition_data = pd.read_csv('data.csv', sep=";")

# Define the columns
numeric_columns = nutrition_data.select_dtypes(include=[np.number]).columns.drop(['Calories'])
non_numeric_columns = nutrition_data.select_dtypes(exclude=[np.number]).columns

# Data Augmentation by adding noise
def augment_data(df, numeric_cols, n_augmented):
    augmented_data = df.copy()
    for _ in range(n_augmented):
        noisy_data = df[numeric_cols].apply(lambda x: x + np.random.normal(0, x.std() * 0.1))
        new_data = df.copy()
        new_data[numeric_cols] = noisy_data
        augmented_data = pd.concat([augmented_data, new_data], ignore_index=True)
    return augmented_data

# Augment the data
augmented_data = augment_data(nutrition_data, numeric_columns, 5)

# Handle non-numeric data and missing values
for col in non_numeric_columns:
    augmented_data[col] = augmented_data[col].astype('category').cat.codes

# Impute missing values
imputer = SimpleImputer(strategy='mean')
X_imputed = imputer.fit_transform(augmented_data[numeric_columns])

# Create a DataFrame with the imputed data to retain column names
X_imputed_df = pd.DataFrame(X_imputed, columns=numeric_columns)

# Standardize features
scaler = StandardScaler()
X_scaled = scaler.fit_transform(X_imputed_df)

# Define the target variable
y = augmented_data['Calories']

# Define the model
model = LinearRegression()

# Define the KFold cross-validator
kf = KFold(n_splits=5, shuffle=True, random_state=42)

# Perform cross-validation and calculate scores
r2_scores = cross_val_score(model, X_scaled, y, cv=kf, scoring='r2')
mae_scores = cross_val_score(model, X_scaled, y, cv=kf, scoring='neg_mean_absolute_error')

print(f"R² Scores: {r2_scores}")
print(f"Mean R² Score: {np.mean(r2_scores)}")
print(f"MAE Scores: {-mae_scores}")
print(f"Mean MAE Score: {-np.mean(mae_scores)}")

# Train-test split for visualization purposes
X_train, X_test, y_train, y_test = train_test_split(X_scaled, y, test_size=0.2, random_state=42)
model.fit(X_train, y_train)
y_pred = model.predict(X_test)

plt.figure(figsize=(10, 6))
sns.histplot(augmented_data['Calories'], kde=True)
plt.title('Distribution of Calories')
plt.xlabel('Calories')
plt.ylabel('Frequency')
plt.show()

# Plotting the relationship between actual and predicted calories
plt.figure(figsize=(10, 6))
plt.scatter(y_test, y_pred, alpha=0.5)
plt.plot([min(y_test), max(y_test)], [min(y_test), max(y_test)], color='red', linestyle='--')
plt.title('Actual vs Predicted Calories')
plt.xlabel('Actual Calories')
plt.ylabel('Predicted Calories')
plt.show()

# Heatmap of the correlation matrix
plt.figure(figsize=(12, 8))
correlation_matrix = augmented_data.corr()
sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', vmin=-1, vmax=1)
plt.title('Correlation Matrix Heatmap')
plt.show()

def calculate_calories(age, sex, weight, height, activity_level):
    if sex == 'male':
        bmr = 10 * weight + 6.25 * height - 5 * age + 5
    else:
        bmr = 10 * weight + 6.25 * height - 5 * age - 161

    if activity_level == 'maintain':
        calories = bmr * 1.2
    elif activity_level == 'lose':
        calories = bmr * 0.8
    elif activity_level == 'gain':
        calories = bmr * 1.5
    else:
        calories = bmr

    return calories

def generate_weekly_recommendations(age, sex, weight, height, activity_level):
    daily_calories = calculate_calories(age, sex, weight, height, activity_level)
    
    recommendations = []
    for day in range(7):
        daily_recommendations = []
        for index, row in nutrition_data.iterrows():
            food_features = row[numeric_columns].values.reshape(1, -1)
            food_features_df = pd.DataFrame(food_features, columns=numeric_columns)
            food_features_scaled = scaler.transform(imputer.transform(food_features_df))  # Impute missing values and scale features
            predicted_calories = model.predict(food_features_scaled)[0]
            if predicted_calories <= daily_calories:
                # Check if the food name contains numbers (likely a measure)
                if not any(char.isdigit() for char in row['Food']):
                    daily_recommendations.append(row['Food'])
        recommendations.append(daily_recommendations)
    
    return recommendations

# Example usage
age, sex, weight, height, activity_level = 30, 'male', 70, 175, 'maintain'
print(generate_weekly_recommendations(age, sex, weight, height, activity_level))
